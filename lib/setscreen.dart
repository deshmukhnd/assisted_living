import 'package:flutter/material.dart';

class SetScreen extends StatefulWidget {
  static const routeName='\setscreen';
  @override
  _SetScreenState createState() => _SetScreenState();
}

class _SetScreenState extends State<SetScreen> {
  Widget sections(String text){
    return Card(child: Text(text),);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(children: <Widget>[
        sections('Privacy Policy'),
        sections('Cancellation Policy'),
        sections('Terms and Condition'),
        ListTile(
          title:Text('Application Version'),
          subtitle: Text('1.0.1'),)
      ],),
    );
  }
}