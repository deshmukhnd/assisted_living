import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AssistedSearch extends StatefulWidget {
  @override
  _AssistedSearchState createState() => _AssistedSearchState();
}

class _AssistedSearchState extends State<AssistedSearch> {
  @override
  void initState() {
    super.initState();
  }

  TextEditingController cityNameController = TextEditingController();
  var _formKey = GlobalKey<FormState>();
  int counter = 1;
final List<String> imgList = [
                          " Personal Care",
'Medication Reminder',
'Home Care',
'Companionship',
'Meal Preparation',
'Continuous Care',
'Special programs',
'Light Housekeeping',
                         ];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
              title: Image.asset(
                'Assisted Living',
                fit: BoxFit.contain,
                height: 40,
              ),
            ),
            resizeToAvoidBottomPadding: false,
            body: SingleChildScrollView(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                  Container(
                    //color: Theme.of(context).primaryColor,
                    child: 
                  Form(
                      key: _formKey,
                      // child: Padding(
                      //   padding: EdgeInsets.all(10),
                        child: 
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(height:10),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(borderRadius: const BorderRadius.all(
          const Radius.circular(20.0),
        ),),
                    hintText: 'Enter State',
                    hintStyle: TextStyle(fontSize: 14),
                  ),
                  keyboardType: TextInputType.text,
                 // controller: nameController,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Please enter name';
                    } else {
                      return null;
                    }
                  },
                  onSaved: (String value) {
                   // nameController.text = value;
                  },
                ), SizedBox(
                  height: 10,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(borderRadius: const BorderRadius.all(
          const Radius.circular(20.0),
        ),),
                    hintText: 'Enter City',
                    hintStyle: TextStyle(fontSize: 14),
                  ),
                  keyboardType: TextInputType.text,
                 // controller: nameController,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Please enter name';
                    } else {
                      return null;
                    }
                  },
                  onSaved: (String value) {
                   // nameController.text = value;
                  },
                ),
                SizedBox(height:10),
                Container(
                   height:50,
                   child: RaisedButton(
                    color: Theme.of(context).primaryColor,
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(100.0)),
                    child:Text('Search',textAlign: TextAlign.center,style: 
                    TextStyle(color:Colors.white,
                    fontSize: 14,
                    fontWeight: FontWeight.bold)),
                    onPressed: (){}
                    ),
                 ),
              ]),
                          )
                        
                        // child: Card(
                        //   shape: RoundedRectangleBorder(
                        //     borderRadius: BorderRadius.circular(15.0),
                        //   ),
                        //   child: 
                          // ListTile(
                          //   leading: Icon(Icons.search),
                          //   title: TextFormField(
                          //     decoration: InputDecoration(
                          //         hintText: "Search city here",
                          //         border: InputBorder.none),
                          //     onTap: () {},
                          //     // keyboardType: TextInputType.text,
                          //     controller: cityNameController,
                          //     textInputAction: TextInputAction.search,

                          //     validator: (String value) {
                          //       if (value.isEmpty) {
                          //         return 'Please enter city name';
                          //       } else {
                          //         return null;
                          //       }
                          //     },
                          //     onSaved: (String value) {
                          //       cityNameController.text;
                          //     },
                          //   ),
                          //   trailing: new IconButton(
                          //     icon: new Icon(Icons.cancel),
                          //     onPressed: () {
                          //       cityNameController.clear();
                          //     },
                          //   ),
                          // ),
                        ),
                         
                      ),
                   // ),
                   
                 // ),

               
                  Container(
                    child: Column(children: [
                      Row(
                        children: [
                          Padding(
                              padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                              child: Text(
                                "What is Assisted living?",
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ))
                        ],
                      ),
                      Column(
                        children: [
                          Padding(
                              padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                              child: Flexible(
                                  child: Text(
                                "Assisted living communities provide 24-hour care for seniors who need help with daily tasks - such as dressing, grooming, and medication management - but don't need ongoing skilled medical services. Assisted living facilities offer a maintenance-free lifestyle that includes restaurant-style meals, wellness programs, housekeeping services, and opportunities for social activities and outings. Some assisted living communities also offer amenities such as gyms, beauty salons and barber shops, and space for gardening",
                              )))
                        ],
                      )
                    ]),
                  ),
                  Row(
                    children: [
                      Padding(
                          padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                          child: Text(
                            "When is it time for Assisted living?",
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ))
                    ],
                  ),
                  Column(
                    children: [
                      Padding(
                          padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                          child: Flexible(
                              child: Text(
                            "Asking yourself and others questions can help determine the right time to transition your aging parent to an assisted living facility. Consider whether your loved one can manage personal care and medications on their own, whether they're safe in their home, and whether they would benefit from a more social and engaging lifestyle. Also weigh your own mental and physical health.Many families struggle with talking to senior loved ones about leaving their long-time home and moving to senior care. Keep them as involved as possible in the planning process and be sure to let your parent know their safety is your main priority. Key factors to improve the senior care conversation include: emphasizing that you want what is best for your parent's safety, picking an appropriate time and place, and, whenever possible, keeping the senior involved in each step of the planning process.",
                          )))
                    ],
                  ),
                  Row(
                    children: [
                      Padding(
                          padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                          child: Text(
                            "How much does Assisted living cost?",
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ))
                    ],
                  ),
                  Column(
                    children: [
                      Padding(
                          padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                          child: Flexible(
                              child: Text(
                            "Assisted living costs vary depending on location, community amenities, apartment size, and how much assistance your loved one needs. You can save money on assisted living costs by carefully weighing the benefits of all-inclusive vs. a la carte care services and asking about move-in specials or other discounts While Medicare does not pay for assisted living, there are many financing options to help pay for assisted living, including veterans benefits, long-term care insurance, reverse mortgages, and more.",
                          )))
                    ],
                  )
                ])
                )));
  }
}
