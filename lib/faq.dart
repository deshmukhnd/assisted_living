import 'package:flutter/material.dart';

class Mydata{
  String _title,_body;
  bool _expand;
   Mydata(this._title,this._body,this._expand);
}

class FaqScrreen extends StatefulWidget {
  static const routeName='\faqscreen';
  @override
  _FaqScrreenState createState() => _FaqScrreenState([
    Mydata('What is an adult family home?','An adult family home is a residential home where up to ${
      'six residents are being cared for.'}',false),
    Mydata('What is the difference between an adult Family Home and Assisted Living?','An Adult family home provides ${
    'more personalized crae in a more home-like setting rather than having the feel of a large and impersonal faculty'}'
    ,false),
    Mydata('How is the registry different than referral agencies?','Unlike referral agecies, which steer you to wards the homes ${
    'the homes of choosing,we provide you with the contact information of the homes that interest you,and give you the'}${
      'freedom to decide which of the homes you should contact.'}',false),
    Mydata('How do I know which Adult Family Home will work best for my parent?','You can dirctly contact Adult Family${
      'Homes in your area and filter them according to the services provided(based on the needs of your parent).'
    }',false),
    Mydata('How do Adult Family homes stay accountable to the state?','The state of Washington conducts yearly${
      'surprise inspections to ensure that Adult Family Homes are operating at optimum level.'}',false),
    Mydata('What is the next step after I contact some Adult Family Homes?','After learning a little bit about their${
      'home over the phone,you can schedule a tour with a few of the homes you feel most comfartable with'}',false),
    Mydata('What should I except during a home tour?','During the tour the AFH provide will walk you though${
      'the home,show you the available rooms,and explain in detail,how the home will meet the needs of your parents'
    }',false),
    Mydata('Are there services that an Adult Family Home does not provide?','Adult Family HOmes are equipped to provide a wide ${
      'range of services for th ederly. MOst notable is the 24 hour care,with assistance of all activities of everyday'}${
        'living.Most homesprovide assistance for dementia and Mental health.Some homes are RN owned and operated. '
    }',false),
    Mydata('How much care should my parent anticipatefrom an Adult Family Home?','Adult Family Homes provide as much or as little${
      'care as the resident requires.Residents that are more independent will be encouraged to use their abilities'}${
        'while those needing assistance will be helped more'}',false),
    Mydata('Who takes care of the residents in an Adult Family Home?','Most Adult Family Homes are either run by the owner${
      '(provider) or they have a residents manager that oversees the house and a care staff that deals with the '}${
        'care and day to day activity of the home.'}',false),
    Mydata('What does a typical day in an Adult Family Home look like?','while each homes is different,below you can${
      'see some general points that appear in a typical day.Residents are assisted with dress and grooming before serving'
    }${'breakfast.Rooms and laundry are cleaned. Activities and exercises  are encouraged. Toileting my take place either'}${
      'Lunch is served followed by time of relaxtion or more organized activities and games.In the evening there is'}${
        'dinner and the day begins to wind down by watching ones favorite show before being assisted with the bebdtime routine '}',false),
    Mydata('How often can I visit my parent in an Adult Family Home?','Families can visitas much or as little as they please.${
      'Family interaction is to encourage and the Adult Family Home Coordinates contact between family and resident'
    }',false),
    Mydata('How can I unsubscribe the home?','For unsubscribe the home,login your account then click on subscription details and ${
      'and unsubscribe tab.You need to fill the feedback form after unsubscribing the home'
    }',false),
  ]);
}

class _FaqScrreenState extends State<FaqScrreen> {
  List<Mydata> _list;
  _FaqScrreenState(this._list);
  _onExpansion(int index,bool isExpanded){
    setState(() {
      _list[index]._expand=!(_list[index]._expand);
    });
  }

  
  @override
  Widget build(BuildContext context) {
    List<ExpansionPanel> mylist=[];
    for(int i=0,ii=_list.length;i<ii;i++){
      var expansiondata=_list[i];
      mylist.add(ExpansionPanel(headerBuilder: (BuildContext context,bool isExpansion){
        return Padding(
          padding:EdgeInsets.all(20.0),
          child:Text(expansiondata._title,
          style:TextStyle(fontSize: 20.0,)));
          },
          body:Padding(padding: EdgeInsets.all(20.0),
          child:Text(expansiondata._body,
          style:TextStyle(fontSize: 15.0,))
          ),
          isExpanded: expansiondata._expand,
          ));
    }
    return Scaffold(
      appBar: AppBar(title:Text('Faq')
      ),
        body: SingleChildScrollView(
        child: Container(
          child:new ExpansionPanelList(
          children:mylist,
          expansionCallback: _onExpansion,
          )
            ),
      ),
    );
  }
}