import 'package:flutter/material.dart';

class CompareScreen extends StatefulWidget {
  @override
  _CompareScreenState createState() => _CompareScreenState();
}

class _CompareScreenState extends State<CompareScreen> {
  Widget tablesection(String text1,Table td){
    return Card(
        child: Column(
          children: <Widget>[
            Center(child:Text(text1)),
            Padding(
            padding: const EdgeInsets.all(8.0),
            child: td,
      ),
          ],
        ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title:Text('Compare')
        ),
        body: ListView(
          padding: EdgeInsets.all(8.0),
          children: <Widget>[
              Text('Info! Choose at leats two cities of ${'your choice to see how they compare'}${
                'on aminities,activities & categories'
              }',style:TextStyle(fontSize:20)),
              tablesection( '',Table(
                  border: TableBorder.all(width:1,color:Colors.grey),
                  children: [
                  TableRow(
                    children: [
                      Text('City'),
                      Text('Select City'),
                      Text('Select City'),
                      Text('Select City')
                    ]
                  ),
                  TableRow(
                    children:[
                      Text('AFH Home'),
                      Text('Select city first'),
                      Text('Select city first'),
                      Text('Select city first'),
                    ])
                  ]),),
            tablesection('Room Types',Table(
              border: TableBorder.all(width:1,color:Colors.grey),
              children: [
              TableRow(children:[
                Text('Private Room'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Semi-Private Room'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Room & Bath'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Room & Shower'),
                Text(''),Text(''),
                Text(''),
              ]),
            ],)),
            tablesection('AFH Services',Table(
              border: TableBorder.all(width:1,color:Colors.grey),
              children: [
              TableRow(children:[
                Text('24 hour care'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Massage Therapist'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('RN Operated'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Dementia/Alzheimer\'s'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Parkinson\'s'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Oxygen Therapy'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Diabetes'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Development Disabilities'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Visual/Hearing Impaired'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Hospice'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Pt/OT & SP Therapy'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Stroke'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Wound care'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Respite Care-Short-term'),
                Text(''),Text(''),
                Text(''),
              ]),
                TableRow(children:[
                Text('Awake Night Staff'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Home Doctor'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Housekeeping/Laundry'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Asst.with daily living'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('medication Management'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Podiatrist'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Medication Delivery'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Pet Care'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Doctor on Site'),
                Text(''),Text(''),
                Text(''),
              ]),
            ],)),
            tablesection('AFH Activities and Amenities',Table(
              border: TableBorder.all(width:1,color:Colors.grey),
              children: [
              TableRow(children:[
                Text('Activities Coordinator'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Arts and Crafts'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Music'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Games'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Birthday Celebration'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Library on Wheel'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Cable TV'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Beutician'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Internet Acceess'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Telephone'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Newspaper'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Wheelchair accessible'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('TV in room'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Emergency Call Button'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Generator'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Hoyer Lift'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Hospital Electric bed Available in the room'),
                Text(''),Text(''),
                Text(''),
              ]),
              TableRow(children:[
                Text('Outdoor Common Areas'),
                Text(''),Text(''),
                Text(''),
              ]),
            ],)),
          ],
        ),
    );
  }
}

