import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import 'package:selection/home_screen.dart/home.dart';
import 'package:assisted_living/tabBar.dart';
//import 'Home.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(
      Duration(seconds: 3),
      () {
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
            builder: (BuildContext context) =>TabBarScreen(),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(child:  Scaffold(
     backgroundColor: Theme.of(context).primaryColor,
      body: SingleChildScrollView(
         child: 
     Column(children:<Widget>[
       Container( 
        color:  Theme.of(context).primaryColor,
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          ClipRect(
            child: Image.asset(
            // 'Icons/Splashscreenlogowhite.png',
            "",
              height:  MediaQuery.of(context).size.height,

            ),
            
          ),
         
        ],
      )),
     ]))
    ));
  }
}
