import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'splash_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
  runApp(MyApp());
}


class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: const Color(0xFF394D67),
          accentColor: const Color(0xFF97C160),
          primaryColorDark: Color(0xFF538DB8),
         // canvasColor: Colors.grey[200],
         // canvasColor: Colors.grey[200],
          //fontFamily: 'OpenSans-Regular',
        ),
        initialRoute: '/',
        routes: {
          '/': (context) => SplashScreen(),
          // '/': (context) => LoginPage(),
        });
  }
}





// // import 'package:afh_app/Models/newplaces.dart';
// // import 'package:afh_app/Models/places.dart';
// import 'faq.dart'; 
// import 'help.dart';
// import 'hiw.dart';
// import 'loginscreen.dart';
// import 'setscreen.dart';
// import 'tabBar.dart';
// import 'tabscreen2.dart';
// import 'package:flutter/material.dart';
// import 'package:selection/tabBar.dart';

// import 'Models/newplaces.dart';
// import 'Models/places.dart';

// void main() {
//   runApp(MyApp());
// }

// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }

// class _MyAppState extends State<MyApp> {
//   List<Place> availableplaces=items;

//   @override
//   Widget build(BuildContext context) {
//     return  MaterialApp(
//         title: 'Flutter Demo',
//         theme: ThemeData(
//           primarySwatch: Colors.blue,
//           visualDensity: VisualDensity.adaptivePlatformDensity,
//         ),
//         home:TabBarScreen(),
//         routes: {
//           LoginScreen.routeName:(ctx)=>LoginScreen(),
//           // FaqScrreen.routeName:(ctx)=>FaqScrreen(),
//           // HIWScreen.routeName:(ctx)=>HIWScreen(),
//           // SetScreen.routeName:(ctx)=>SetScreen(),
//           // HelpScreen.routeName:(ctx)=>HelpScreen(),
//           // Listofhouses.routeName:(ctx)=>Listofhouses(),
//         },
//     );
//   }
// }

