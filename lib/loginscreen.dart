

import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  static const routeName='\loginscreen';
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _usernamecontroller=TextEditingController();
  final _passwordcontroller=TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Center(
        child:Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
               SizedBox(
                        height: 100.0,
                        child: Image.asset(
                          "assets/images/logoNew.png",
                          fit: BoxFit.contain,
                        ),),
                        SizedBox(height:50),
            TextField(
              controller: _usernamecontroller,
              decoration: 
            InputDecoration(
              labelText:'Enter Username',
              
              prefixIcon: Icon(Icons.account_circle),
              border:
              OutlineInputBorder(
                borderRadius: BorderRadius.circular(80.0))
            ),
            ),
            SizedBox(
              height:10
            ),
            TextField(
              obscureText: true,
              controller: _passwordcontroller,
              decoration: 
            InputDecoration(
              labelText:'Enter Password',
              prefixIcon: Icon(Icons.vpn_key),
              border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(80.0)),
              ),
              ),
              SizedBox(height:10),
                 Container(
                   height:50,
                   child: RaisedButton(
                    color: Colors.green,
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(100.0)),
                    child:Text('Login',textAlign: TextAlign.center,style: 
                    TextStyle(color:Colors.white,
                    fontSize: 25,
                    fontWeight: FontWeight.bold)),
                    onPressed: (){}
                    ),
                 ),
          ],),
        )
      )
    );
  }
}