
// import 'package:afh_app/faq.dart';
// import 'package:afh_app/help.dart';
// import 'package:afh_app/hiw.dart';
// import 'package:afh_app/setscreen.dart';
import 'package:assisted_living/setscreen.dart';

import 'faq.dart';
import 'help.dart';
import 'hiw.dart';
import 'loginscreen.dart';
import 'package:flutter/material.dart';

class SettingScreen extends StatefulWidget {
  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  Widget section(String text1,Icon wd,Function m2){
   return GestureDetector(
     onTap:m2,
      child: Card(
       elevation: 2.0,
          child: ListTile(
          leading: wd,
          title: Text(text1),
            ),
     ),
   );
}
void _jump(){
  setState(() {
    Navigator.of(context).pushNamed(LoginScreen.routeName);
  });
}
void _jump1(){
  setState(() {
    Navigator.of(context).pushNamed(SetScreen.routeName);
  });
}
void _jump2(){
  setState(() {
    Navigator.of(context).pushNamed(HIWScreen.routeName);
  });
}
void _jump3(){
  setState(() {
    Navigator.of(context).pushNamed(FaqScrreen.routeName);
  });
}
void _jump4(){
  setState(() {
    Navigator.of(context).pushNamed(HelpScreen.routeName);
  });
}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar:PreferredSize(
          child: AppBar(
          title:Text('Login to explore'),
          // actions: <Widget>[
          //   Text('Get Started')
          // ],
        ),
         preferredSize:Size.fromHeight(100),
         ),  
        body: ListView(
        children: <Widget>[
          section('Tour Request',Icon(Icons.notifications,color: Theme.of(context).primaryColor,),_jump),
          section('Callback request',Icon(Icons.remove_circle,color: Theme.of(context).primaryColor,),_jump),
          section('Deposit History',Icon(Icons.history,color: Theme.of(context).primaryColor,),_jump),
          section('Favorites',Icon(Icons.favorite,color: Theme.of(context).primaryColor,),_jump),
          section('Help Center & Feedback',Icon(Icons.help,color: Theme.of(context).primaryColor,),_jump4),
          section('How it works',Icon(Icons.loop,color: Theme.of(context).primaryColor,),_jump2),
          section('FAQ\'s',Icon(Icons.announcement,color: Theme.of(context).primaryColor,),_jump3),
          section('Settings',Icon(Icons.settings,color: Theme.of(context).primaryColor,),_jump1),
          section('Logout',Icon(Icons.exit_to_app,color: Theme.of(context).primaryColor,),_jump),
        ],
      ),
    );
  }
}