//import 'package:afh_app/Models/places.dart';
//import 'package:afh_app/tabscreen2.dart';
import 'package:flutter/material.dart';
import 'package:assisted_living/tabscreen2.dart';

import 'Models/places.dart';

class Houseslist extends StatelessWidget {
  final List<Place> availableplace;
  Houseslist(this.availableplace);
  static const routeName='\houseslist';
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
          itemCount: availableplace.length,
          itemBuilder: (ctx,index){
          return ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: GridTile(
          child: GestureDetector(
          onTap: (){
            Navigator.of(context).pushNamed(Listofhouses.routeName);
          },  
          child: Image.network(availableplace[index].imageurl,fit: BoxFit.cover,
)),
          footer: Column(
          children: <Widget>[
            Text(availableplace[index].title),
            Text(availableplace[index].city),
          ],
          )
          ),
      );
          },
    );
  }
}
  