import 'package:flutter/material.dart';
import 'faq.dart';

class HIWScreen extends StatefulWidget {
  static const routeName='\hiwscreen';
  @override
  _HIWScreenState createState() => _HIWScreenState();
}

class _HIWScreenState extends State<HIWScreen> {
  Widget steps(String text,Image id,String text1,String text2){
    return Card(
      child: Padding(
          padding:EdgeInsets.all(10.0),
          child: Column(
         // mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
          //SizedBox(height:10),
          Text(text,style:TextStyle(fontSize:18)),
          SizedBox(height:10),
          ListTile(
            leading:id,
            title:Text(text1,style:TextStyle(decoration: TextDecoration.underline,fontSize: 20)),
            subtitle: Text(text2),
            ),
        ],),
      ));
  }
   Widget steps2(String text,Image id,String text1,String text2){
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
          //SizedBox(height:10),
          Text(text,style:TextStyle(fontSize:18)),
          SizedBox(height:10),
          ListTile(
            title:Text(text1,style:TextStyle(decoration: TextDecoration.underline,fontSize: 20)),
            subtitle: Text(text2),
            trailing:id,
            ),
        ],),
      ));
  }
  void jump(){
    setState(() {
   Navigator.of(context).pushNamed(FaqScrreen.routeName);   
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title:Text('How it Works')),
      body:ListView(
        children: <Widget>[
            steps('Step 1',Image.asset('assets/images/1Search.png'),'Search',
            'Search adult family homes by city and zip code'),
            steps2('Step 2', Image.asset('assets/images/1Filter.png'), 'Filter',
            'Compare homes and select services according to your needs.'),
            steps('Step 3',Image.asset('assets/images/1Connect.png'),'Connect',
            'Get in touch directly with the adult family home with any question'),
            steps2('Step 4',Image.asset('assets/images/1Move.png'),'Move In',
            'Follow online steps to set up a tour of the home and to hold a or book a room'),
            SizedBox(height:10),
            Container(
                  child : GestureDetector(
                  onTap: jump,
                  child: Card(
                  color: Colors.green,
                  child: ListTile(
                  title: Text('FAQ\'s'),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),),
              ),
            ),
        ],
      ));
  }
}