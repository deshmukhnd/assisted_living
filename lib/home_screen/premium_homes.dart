import 'dart:convert';
import 'package:flutter/material.dart';

import 'package:page_transition/page_transition.dart';
import '../constant.dart';


class PremiumHomes extends StatefulWidget {
  @override
  _PremiumHomesState createState() => _PremiumHomesState();
}
class _PremiumHomesState extends State<PremiumHomes> {
  @override
  void initState() {
    super.initState();
 
  }

  

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.fromLTRB(5, 10, 0, 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 5),
            child: Text(
              'Premium Homes',
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          SizedBox(height: 10),
          Container(
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                   
                  },
                  child: Card(
                    shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                       ),
                    elevation: 5,
                    color: Colors.white,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        ClipRRect(
                          borderRadius:
                        
                          BorderRadius.only(topLeft:Radius.circular(20),topRight: Radius.circular(20) ),
                          child: new Image.network(
                           'https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1550868101/afh/listinghomes/singleimages/srrldhh9i6epys5otjvz.jpg',
                            fit: BoxFit.fill,
                            
                            height: 100,
                            width: 150,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                          width: 150,
                          padding: EdgeInsets.only(left: 5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: 5,
                              ), 
                             Row(children: <Widget>[
                               Flexible(child: 
                              Text(
                                "Bridle trial senior care" ,
                               
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold,                 
                                ),
                               
                              ),)],),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "Kent" ,
                                style: TextStyle(
                                  fontSize: 11,
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
              //itemCount: premiumHomeData == null ? 0 : premiumHomeData.length,
            ),
            height: 200,
           
          
          ),
        ],
      ),
    );
  }
}
