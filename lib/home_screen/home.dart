import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:assisted_living/Models/newplaces.dart';
import 'package:assisted_living/categories/assisted_search.dart';

import 'premium_homes.dart';

import 'cities.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
  }

  var img, data;
  int tq, cb, msg;
  int noti;

  List popularHomesData = List();
  var _formKey = GlobalKey<FormState>();
  TextEditingController cityNameController = TextEditingController();

  int counter = 1;
  CarouselSlider carouselSlider;

final List<String> imgList = [
                           "https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1595401449/CaliforniaAFH/helping-elderly-people-home-flat-cartoon-composition-with-grandchildren-serving-coffee-cakes-old-grandparents_1284-28616.jpg",
                           'https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1595595727/nursing-home-concept-illustration_114360-2704.jpg',
                           'https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1595596721/volunteers-helping-senior-people_23-2148550222.jpg',
                            "https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1596545687/CaliforniaAFH/woman-taking-care-about-senior-man-with-flu_179970-1750.jpg",
                            "https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1595609303/characters-senior-people-holding-healthcare-icons-illustration_53876-40413.jpg",
                            "https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1596543901/CaliforniaAFH/coronavirus-2019-ncov-covid-19-senior-couple-stay-home-awareness-social-media-campaign-coronavirus-prevention-happy-family-lifestyle-activity-staying-together-home-white-background_83111-661.jpg",
                         ];
  Future<bool> _onWillPop() {
    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Are you sure?'),
            content: Text('Do you want to exit an App'),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text('No'),
              ),
              FlatButton(
                onPressed: () => exit(0),
                child: Text('Yes'),
              ),
            ],
          ),
        ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onWillPop,
        child: SafeArea(
            child: Scaffold(
          appBar: AppBar(
              title: Image.asset(
                'Icons/Adultfamilyhomelogowhite.png',
                fit: BoxFit.contain,
                height: 40,
              ),
              actions: <Widget>[
                new Stack(children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.notifications),
                    onPressed: () {
                      Text("notification");
                    },
                  ),
                  new Positioned(
                    right: 11,
                    top: 11,
                    child: new Container(
                      padding: EdgeInsets.all(2),
                      decoration: new BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(6),
                      ),
                      constraints: BoxConstraints(
                        minWidth: 14,
                        minHeight: 14,
                      ),
                      child: Text(
                        noti == null ? "0" : "$noti",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 8,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                ])
              ]),
          resizeToAvoidBottomPadding: false,
          body: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Container(
                  color: Theme.of(context).primaryColor,
                  child: Form(
                    key: _formKey,
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: ListTile(
                            leading: Icon(Icons.search),
                            title: TextFormField(
                              decoration: InputDecoration(
                                  hintText: "Search city/Home here",
                                  border: InputBorder.none),
                              onTap: () {},
                              // keyboardType: TextInputType.text,
                              controller: cityNameController,
                              textInputAction: TextInputAction.search,

                              validator: (String value) {
                                if (value.isEmpty) {
                                  return 'Please enter city name';
                                } else {
                                  return null;
                                }
                              },
                              onSaved: (String value) {
                                cityNameController.text;
                              },
                            ),
                            trailing: new IconButton(
                              icon: new Icon(Icons.cancel),
                              onPressed: () {
                                cityNameController.clear();
                              },
                            ),
                          )),
                    ),
                  ),
                ),
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                     CarouselSlider.builder(
          itemCount: imgList.length,
          options: CarouselOptions(
            autoPlay: true,

            aspectRatio: 2.0,
            enlargeCenterPage: true,
          ),
          itemBuilder: (context, index) {
            return Container(
              
              margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                                 decoration: BoxDecoration(
                                  // shape: BoxShape.circle,
                                     borderRadius: BorderRadius.circular(20.0),
                                     //color: Colors.blueGrey
                                    ), 
              child: ClipRRect(
                
                 borderRadius: BorderRadius.all(
                                              Radius.circular(10.0),
                 ),
                child: Image.network(imgList[index], fit: BoxFit.fitWidth, width: 2000,semanticLabel: "text",)
                
              ),
            );
          },
        )
                      
                    ],
                  ),
                ),
                Container(
                  color: Colors.white,
                  padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                  child: Cities(),
                ),
                SizedBox(
                  height: 10,
                ),
                GestureDetector(onTap: () {
                  Navigator.push(
                            context,
                            PageTransition(
                                type: PageTransitionType.rightToLeft,
                                child: AssistedSearch(
                                 
                                )),
                          );
                }, child:Card(
                    color: Colors.amber,
                    margin: EdgeInsets.fromLTRB(10, 0, 10, 5),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          ClipRRect(
                            // borderRadius: BorderRadius.all(Radius.circular(10)),
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                topRight: Radius.circular(10)),
                            child: new Image.network(
                              //"" + finalData[index]['imagePath'],
                              "https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1595401449/CaliforniaAFH/helping-elderly-people-home-flat-cartoon-composition-with-grandchildren-serving-coffee-cakes-old-grandparents_1284-28616.jpg",
                              fit: BoxFit.cover,
                              height: 150,
                              // width: 400,
                              width: MediaQuery.of(context).size.height,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Padding(
                                  padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                                  child: Text(
                                      "Tap here to search assisted living homes"))
                            ],
                          )
                        ])
                        ),),
                         SizedBox(
                  height: 10,
                ),
                Card(
                    color: Colors.amber,
                    margin: EdgeInsets.fromLTRB(10, 0, 10, 5),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          ClipRRect(
                            // borderRadius: BorderRadius.all(Radius.circular(10)),
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                topRight: Radius.circular(10)),
                            child: new Image.network(
                              //"" + finalData[index]['imagePath'],
                              'https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1595595727/nursing-home-concept-illustration_114360-2704.jpg'
                              ,fit: BoxFit.cover,
                              height: 150,
                              // width: 400,
                              width: MediaQuery.of(context).size.height,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Padding(
                                  padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                                  child: Text(
                                      "Tap here to search assisted living homes"))
                            ],
                          )
                        ])),
                         SizedBox(
                  height: 10,
                ),
                Card(
                    color: Colors.amber,
                    margin: EdgeInsets.fromLTRB(10, 0, 10, 5),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          ClipRRect(
                            // borderRadius: BorderRadius.all(Radius.circular(10)),
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                topRight: Radius.circular(10)),
                            child: new Image.network(
                              //"" + finalData[index]['imagePath'],
                             'https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1595596721/volunteers-helping-senior-people_23-2148550222.jpg',
                              fit: BoxFit.cover,
                              height: 150,
                              // width: 400,
                              width: MediaQuery.of(context).size.height,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Padding(
                                  padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                                  child: Text(
                                      "Tap here to search assisted living homes"))
                            ],
                          )
                        ])),
                         SizedBox(
                  height: 10,
                ),
                Card(
                    color: Colors.amber,
                    margin: EdgeInsets.fromLTRB(10, 0, 10, 5),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          ClipRRect(
                            // borderRadius: BorderRadius.all(Radius.circular(10)),
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                topRight: Radius.circular(10)),
                            child: new Image.network(
                              //"" + finalData[index]['imagePath'],
                             "https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1596545687/CaliforniaAFH/woman-taking-care-about-senior-man-with-flu_179970-1750.jpg",
                              fit: BoxFit.cover,
                              height: 150,
                              // width: 400,
                              width: MediaQuery.of(context).size.height,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Padding(
                                  padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                                  child: Text(
                                      "Tap here to search assisted living homes"))
                            ],
                          )
                        ])),
                Container(
                  color: Colors.white,
                  child: PremiumHomes(),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  color: Colors.white,
                  padding: EdgeInsets.fromLTRB(5, 10, 0, 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(left: 5),
                        child: Text(
                          'Popular Homes',
                          style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                              onTap: () {},
                              child: Card(
                                // shape: RoundedRectangleBorder(
                                //   borderRadius: BorderRadius.circular(0.0),
                                // ),
                                // elevation: 5,
                                color: Colors.white,
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    ClipRRect(
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(5),
                                          topRight: Radius.circular(5)),
                                      child: new Image.network(
                                        'https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1550868101/afh/listinghomes/singleimages/srrldhh9i6epys5otjvz.jpg',
                                        fit: BoxFit.fill,
                                        height: 100,
                                        width: 150,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    // Container(
                                    //   width: 150,
                                    //   padding: EdgeInsets.only(left: 5),
                                    //   child: Column(
                                    //     crossAxisAlignment:
                                    //         CrossAxisAlignment.start,
                                    //     children: <Widget>[
                                    //       SizedBox(
                                    //         height: 5,
                                    //       ),
                                    //       Row(
                                    //         children: <Widget>[
                                    //           Flexible(
                                    //             child: Text(
                                    //               "Bridle Senior Care" ,
                                    //               overflow:
                                    //                   TextOverflow.ellipsis,
                                    //               style: TextStyle(
                                    //                 fontSize: 12,
                                    //                 fontWeight:
                                    //                     FontWeight.bold,
                                    //                 // color: Theme.of(context)
                                    //                 //                 .primaryColorDark
                                    //               ),
                                    //             ),
                                    //           )
                                    //         ],
                                    //       ),
                                    //       SizedBox(
                                    //         height: 5,
                                    //       ),
                                    //       Text(
                                    //         "Kent" ,
                                    //         style: TextStyle(
                                    //           fontSize: 11,
                                    //         ),
                                    //       ),
                                    //       SizedBox(
                                    //         height: 10,
                                    //       ),

                                    //     ],
                                    //   ),
                                    // ),
                                  ],
                                ),
                              ),
                            );
                          },
                          // itemCount: popularHomesData == null
                          //     ? 0
                          //     : popularHomesData.length
                        ),
                        height: 200,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        )));
  }
}


