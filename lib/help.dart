import 'package:flutter/material.dart';

class HelpScreen extends StatefulWidget {
  static const routeName='\help';
  @override
  _HelpScreenState createState() => _HelpScreenState();
}

class _HelpScreenState extends State<HelpScreen> {
  Widget centers(String text,Icon wd){
    return Container(
      //width:50,
      height:50,
      child: Card(child: Row(children: <Widget>[
        wd,
        SizedBox(width:10),
        Text(text,style:TextStyle(fontSize:20)),
      ],),),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
      title:Text('Help center')),
      body: Column(children: <Widget>[
        centers('Contact Us',Icon(Icons.rate_review,color:Theme.of(context).primaryColor)),
        centers('Rate Us',Icon(Icons.star,color:Theme.of(context).primaryColor)),
     ],),
    );
  }
}