// import 'package:afh_app/compare.dart';
// import 'package:afh_app/home.dart';
// import 'package:afh_app/msgscreen.dart';
// import 'package:afh_app/setting.dart';
import 'package:flutter/material.dart';

import 'compare.dart';
import 'home_screen/home.dart';
import 'msgscreen.dart';
import 'setting.dart';


class TabBarScreen extends StatefulWidget {
  
   
  @override
  _TabBarScreenState createState() => _TabBarScreenState();
}

class _TabBarScreenState extends State<TabBarScreen> {
  List _pages=[
    HomeScreen(),
    CompareScreen(),
    MsgScreen(),
    SettingScreen(),
  ];
   int _selectindex=0;
  void _selectpage(int index){
   setState(() {
     _selectindex=index;
   });
 }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:_pages[_selectindex],
      bottomNavigationBar: BottomNavigationBar(
      selectedItemColor: Theme.of(context).primaryColor,
      currentIndex: _selectindex,
      type: BottomNavigationBarType.fixed,
      unselectedFontSize:10 ,
      unselectedItemColor: Colors.grey,
      onTap: _selectpage,
      items: [
        BottomNavigationBarItem(icon: Icon(Icons.home),title: Text('Home')),
        BottomNavigationBarItem(icon: Icon(Icons.compare),title: Text('Compare')),
        BottomNavigationBarItem(icon: Icon(Icons.message),title:Text('Message')),
        BottomNavigationBarItem(icon: Icon(Icons.account_circle),title:Text('Account')),
      ]),
    );
  }
}