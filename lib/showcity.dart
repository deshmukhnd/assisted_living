//import 'package:afh_app/tabscreen2.dart';
import 'package:flutter/material.dart';
import 'package:assisted_living/tabscreen2.dart';


class ShowCity extends StatelessWidget {
  final String cityname;
  final String imageurl;
  ShowCity({@required this.cityname,@required this.imageurl});
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      SizedBox(height: 10,),
      GestureDetector(
        onTap: (){Navigator.of(context).pushNamed(Listofhouses.routeName);},
          child: ClipOval(
          child: Image.network(imageurl,
          height:80,
          width:80,
          fit:BoxFit.cover,
          ),
        ),
      ),
      SizedBox(width:8),
      SizedBox(height:5),
      Text(cityname),
    ],);
  }
}