import 'package:flutter/foundation.dart';
class Category{
  final String id;
  final String title;
  final String imageurl;

 const Category({
   @required this.id,
    @required this.title,
    @required this.imageurl});
}
class Place {
  final String title;
  final String address;
  final String imageurl;
  final bool medicaid;
  final bool premium;
  final String owner;
  final String country;
  final String city;
  final int zipcode;
  final String phoneno;

 const Place({    
    @required this.title,
    @required this.address,
    @required this.imageurl,
    @required this.medicaid,
    @required this.premium,
    @required this.owner,
    @required this.country,
    @required this.city,
    @required this.zipcode,
    @required this.phoneno,
   } );

}