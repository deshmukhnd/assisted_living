//import 'package:afh_app/Models/places.dart';
import 'package:assisted_living/Models/places.dart';

  const cities=const[
    Category(
      id: 'c1',
      title: 'Kent',
      imageurl: '',),
      Category(
        id:'c2',
        title:'Krikland',
        imageurl: 
        'https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1544101822/afh/listinghomes/singleimages/gua1syrmh0m0fwdqxymk.jpg', ),
      Category(
        id: 'c3', 
        title: 'Seattle', 
        imageurl: 
        'https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1552074108/afh/listinghomes/singleimages/uxivzsugft9fvqia3qb8.jpg'),
      Category(
        id: 'c4', 
        title: 'Lynwood', 
        imageurl: 
        'https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1548226634/afh/listinghomes/singleimages/dankdfmnvpkwx1t2obay.jpg'),
      Category(
        id:'c5',
        title: 'Tukwila',
        imageurl: 
        'https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1554929342/afh/listinghomes/singleimages/qh0tu57hpqcpudzyn4bv.jpg', ),
      Category(
        id: 'c6', 
        title: 'Tumwater', 
        imageurl:
         'https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1554976092/afh/listinghomes/singleimages/eebmyqu0bgvwjlqqcbza.jpg'),
      Category(
        id: 'c7', 
        title: 'Kenmore', 
        imageurl: 
        'https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1554189374/afh/listinghomes/singleimages/gqd5ywmvobpb93e4zk4m.jpg'),
        Category(
        id: 'c8', 
        title: 'Mill Creek', 
        imageurl: 
        'https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1552421273/afh/listinghomes/singleimages/f6wnnepdooaaawluvzle.jpg')
   ];

 const items=const[
    Place(
      title:'A+ MERDIAN VILLA ESTATES',
     address:'23420 124th Avenue Southeast,Kent,WA,USA ', 
     imageurl:'https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1550868101/afh/listinghomes/singleimages/srrldhh9i6epys5otjvz.jpg',
      medicaid:true, 
      premium:true, 
      owner:'MARCELA CUSMIR', 
      country:'USA', 
      city:'Knet', 
      zipcode:98031,
      phoneno:'253-638-0485',
      ),
      Place(
        title:'First Choice Care AFH', 
        address:'22028 108th Ave SE,Kent,WA,USA', 
        imageurl:'https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1544101806/afh/listinghomes/singleimages/tuz2ag9gubazdbiywbcz.jpg',
        medicaid:true,
        premium:true,
        owner:'Daniela Torkelson',
        country:'USA',
        city:'Kent',
        zipcode:98031,
        phoneno:'(253)813-0559',
        ),
        Place(
          title:'Sunrise Care AFH',
          address:'22014 108th Avenue Southeast,Kent,WA,USA',
          imageurl:'https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1544101814/afh/listinghomes/singleimages/utnfdtbghygljfacoafr.jpg',
          medicaid:true,
          premium:false,
          owner:'Dickson Nijiere',
          country:'USA',
          city:'Kent',
          zipcode:98031,
          phoneno:'206-769-0603'
        ),
        Place(
          title:'Harmony & Compassion AHC-LLC',
          address:'23430 98th Avenue South,Kent,WA,USA',
          imageurl:'https://res.cloudinary.com/adultfamilyhomes-org/image/upload/v1556913978/afh/listinghomes/singleimages/rrkm0qhq0vxvicquur2f.png',
          medicaid:false,
          premium:false,
          owner:'Loredana Delean',
          country:'USA',
          city:'Kent',
          zipcode:98031,
          phoneno:'206-850-7489'
        ),
  ];
